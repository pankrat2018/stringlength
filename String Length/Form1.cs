﻿using System;
using System.Windows.Forms;

namespace String_Length
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text.Replace(" ", "");
            label1.Text = "Длина строки: " + textBox1.Text.Length;
        }
    }
}
